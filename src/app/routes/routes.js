var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var hashHistory = ReactRouter.hashHistory;
var IndexRoute = ReactRouter.IndexRoute;
var IndexRedirect = ReactRouter.IndexRedirect;
var BaseLayout = require('components/layouts/Base').default; //Babel 6 need to specify default
var HomePage = require("components/pages/Home").default;

var routes = (
  <Router history={hashHistory}>
    <Route path='/' component={BaseLayout} >
      <IndexRedirect to="/home" />
      <Route path="home" component={HomePage} />
    </Route>
  </Router>
);

module.exports = routes;
