import React, {PropTypes} from 'react';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>Home Page</h1>
        <p>Some content</p>
      </div>

  );
  }
}

Home.propTypes = {
};
